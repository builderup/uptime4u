import 'dart:convert';
import 'dart:async';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class AuthServices{
  Future<Map> login(String login, String password) async{
    final response = await http.post("https://up.uptime.com.br/api/v1/login", body: {"login": login,"password": password});
    return json.decode(response.body);
  }

  Future<bool> logout() async{
    Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
    final SharedPreferences prefs = await _prefs;
    return prefs.remove('token');
  }

  setToken(token) async {
    Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
    final SharedPreferences prefs = await _prefs;
    await prefs.setString('token', token);
  }
  Future<String> getToken() async{
    Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
    final SharedPreferences prefs = await _prefs;
    return prefs.getString('token');
  }

  Future<bool> isLogged() async{
    String token = await getToken();
    return token != null;
  }

  Map<String, dynamic> parseJwt(String token){
    final parts = token.split('.');
    if(parts.length != 3){
      throw Exception('invalid token');
    }
    final payload = _decodeBase64(parts[1]);
    final payloadMap = json.decode(payload);
    if(payloadMap is! Map<String, dynamic>){
      throw Exception('invalid payload');
    }
    return payloadMap;
  }

  String _decodeBase64(String str){
    String output = str.replaceAll('-', '+').replaceAll('_', '/');
    switch(output.length % 4){
      case 0:
        break;
      case 2:
        output += '==';
        break;
      case 3:
        output += '=';
        break;
      default:
        throw Exception('Illegal base64url string!"');
        break;
    }
    return utf8.decode(base64Url.decode(output));
  }
}