import 'package:flutter/material.dart';
import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:transparent_image/transparent_image.dart';
import 'package:uptime4u/pages/post.dart';

class ComunicadosPage extends StatefulWidget {
  @override
  _ComunicadosPageState createState() => _ComunicadosPageState();
}

class _ComunicadosPageState extends State<ComunicadosPage> {

  final String apiUrl = "https://virtuooza.com/wp-json/wp/v2/";
    List posts;
    Future<String> getPosts() async {
      var res = await http.get(Uri.encodeFull(apiUrl + "posts?_embed"), headers: {"Accept": "application/json"});
      // fill our posts list with results and update state
      setState(() {
        var resBody = json.decode(res.body);
        posts = resBody;
      });

      return "Success!";
    }

    @override
    void initState() {
      super.initState();
      this.getPosts();
    }

  @override
  Widget build(BuildContext context) {

    return ListView.builder(
      itemCount: posts == null ? 0 : posts.length,
      itemBuilder: (BuildContext context, int index){
        return Column(
          children: <Widget>[
            GestureDetector(
              child: Card(
                color: Colors.grey[50],
                margin: EdgeInsets.all(16.0),
                child: Column(
                  children: <Widget>[
                    FadeInImage.memoryNetwork(
                      placeholder: kTransparentImage,
                      image: posts[index]['featured_media'] == 0 ? 'http://imprensa.uptime.com.br/wp-content/uploads/2018/12/4U-UPTIME-AWARDS-2018-ULTIMO-DIA-PRIMEIRO-LOTE.jpg' : posts[index]["_embedded"]["wp:featuredmedia"][0]["source_url"],
                    ),
                    Container(
                      padding: EdgeInsets.all(10.0),
                      child: Row(
                        children: <Widget>[
                          Expanded(
                            child: Column(
                              children: <Widget>[
                                Text(
                                  posts[index]["title"]["rendered"],
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    color: Colors.black
                                  ),
                                ),
                                Divider(
                                  height: 16.0,
                                  color: Colors.transparent,
                                ),
                                Text(
                                  posts[index]["excerpt"]["rendered"].replaceAll(new RegExp(r'<[^>]*>'), ''),
                                  style: TextStyle(
                                    fontWeight: FontWeight.w300,
                                    color: Colors.black,
                                    fontSize: 13.0
                                  ),
                                )
                              ],
                            )
                          ),
                        ],
                      )
                    ),
                    Padding(
                      padding: EdgeInsets.only(
                        left: 16.0,
                        right: 16.0,
                        bottom: 16.0
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Icon(
                            Icons.star,
                            color: Colors.red[200],
                            size: 16.0,
                          ),
                          Text(
                            "4",
                            style: TextStyle(
                              color: Colors.red[200],
                              fontSize: 17.0
                            ),
                          ),
                          Text("          "),  
                          Icon(
                            Icons.comment,
                            color: Colors.red[200],
                            size: 15.0,
                          ),
                          Text(
                            "15",
                            style: TextStyle(
                              color: Colors.red[200],
                              fontSize: 17.0
                            ),
                          )
                        ],
                      ),
                    )
                  ],
                ),
              ),
              onTap: (){
                Navigator.push(
                  context, 
                  MaterialPageRoute(
                    builder: (context) => new PostPage(post: posts[index])
                  )
                );
              },
            )
          ],
        );
      },
    );
  }
}