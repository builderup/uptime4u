import 'package:flutter/material.dart';
import 'package:transparent_image/transparent_image.dart';
class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    double _width = MediaQuery.of(context).size.width;
    Text _getTitle(String text){
      return Text(
        text,
        style: TextStyle(
          fontWeight: FontWeight.bold,
          color: Colors.black,
          fontSize: 20.0,
        ),
      );
    }
    
    return SingleChildScrollView(
      child: Column(
        children: <Widget>[
          GestureDetector(
            child: FadeInImage.memoryNetwork(
              placeholder: kTransparentImage,
              image: "http://imprensa.uptime.com.br/wp-content/uploads/2018/12/27-12-2018-UPTIME-ANO-NOVO-HL.jpg",
              width: _width,
              fit: BoxFit.cover,
            ),
            onTap: (){
              // Navigator.push(
              //   context,
              //   MaterialPageRoute(
              //     builder: (context) => PostPage()
              //   )
              // );
            },
          ),
          Divider(
            color: Colors.red,
            height: 16.0,
          ),
          _getTitle("4U Information"),
          GestureDetector(
            child: Padding(
              padding: EdgeInsets.all(10.0),
              child: Card(
                child: Column(
                  children: <Widget>[
                    FadeInImage.memoryNetwork(
                      placeholder: kTransparentImage,
                      image: "http://imprensa.uptime.com.br/wp-content/uploads/2018/12/4U-UPTIME-AWARDS-2018-ULTIMO-DIA-PRIMEIRO-LOTE.jpg",
                      width: _width,
                      fit: BoxFit.cover,
                    ),
                    Container(
                      padding: EdgeInsets.all(10.0),
                      child: Row(
                        children: <Widget>[
                          Expanded(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Container(
                                  padding: EdgeInsets.only(bottom: 5.0),
                                  child: Text(
                                    "4U Information",
                                    style: TextStyle(
                                      fontWeight: FontWeight.w300,
                                      color: Colors.black,
                                      fontSize: 13.0
                                    )
                                  ),
                                ),
                                Text(
                                  "VAI VIRAR: 1º lote chega ao fim",
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    color: Colors.black
                                  ),
                                )
                              ],
                            ),
                          ),
                          Icon(
                            Icons.star,
                            color: Colors.red[200],
                            size: 15.0,
                          ),
                          Text(
                            "4",
                            style: TextStyle(
                              color: Colors.red[200],
                              fontSize: 14.0
                            ),
                          )
                        ],
                      ),
                    )
                  ],
                )
              ),
            ),
            onTap: (){
              // Navigator.push(
              //   context,
              //   MaterialPageRoute(
              //     builder: (context) => PostPage()
              //   )
              // );
            },
          )
        ],
      ),
    );
  }
}