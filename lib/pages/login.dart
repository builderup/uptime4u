import 'package:flutter/material.dart';
import 'package:uptime4u/pages/master.dart';
import 'package:uptime4u/services/auth.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
  TextEditingController _loginController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("UPTIME4U"),
        centerTitle: true,
        backgroundColor: Colors.red,
      ),
      backgroundColor: Colors.white,
      body: Container(
        padding: EdgeInsets.all(16.0),
        child: Center(
          child: Form(
            key: _formKey,
            child: ListView(
              children: <Widget>[
                TextFormField(
                  decoration: InputDecoration(
                    icon: Icon(Icons.person, color: Colors.red),
                    labelText: "Login: *",
                  ),
                  controller: this._loginController,
                  validator: (val){
                    if(val.isEmpty){
                      return "The login is required!";
                    }
                  },
                ),
                Divider(
                  height: 16.0,
                  color: Colors.transparent,
                ),
                TextFormField(
                  obscureText: true,
                  decoration: InputDecoration(
                    icon: Icon(Icons.lock, color: Colors.red),
                    labelText: "Password: *",
                  ),
                  controller: this._passwordController,
                  validator: (val){
                    if(val.isEmpty){
                      return "The password is required!";
                    }
                  },
                ),
                Container(
                  child: RaisedButton(
                    child: Text(
                      "Login",
                      style: TextStyle(
                        color: Colors.white
                      ),
                    ),
                    color: Colors.red,
                    onPressed: (){
                      if(this._formKey.currentState.validate()){
                        AuthServices auth = new AuthServices();
                        auth.login(_loginController.text, _passwordController.text).then((onValue){
                          if(onValue.containsKey('token')){
                            auth.setToken(onValue['token']);
                            Navigator.pushReplacement(
                              context,
                              MaterialPageRoute(
                                builder: (context) => MasterPage()
                              )
                            );
                          }
                        });
                      }
                    },
                  ),
                  margin: EdgeInsets.only(
                    top: 16.0
                  ),
                )
              ],
            ),
          ),
        )
      ),
    );
  }
}