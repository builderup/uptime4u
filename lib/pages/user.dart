import 'package:flutter/material.dart';
import 'package:uptime4u/pages/login.dart';
import 'package:uptime4u/services/auth.dart';
class UserPage extends StatefulWidget {
  @override
  _UserPageState createState() => _UserPageState();
}

class _UserPageState extends State<UserPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("UPTIME4U"),
        backgroundColor: Colors.red,
        centerTitle: true,
      ),
      body: ListView(
        children: <Widget>[
          FlatButton(
            child: Row(
              children: <Widget>[
                Icon(
                  Icons.sync,
                  size: 23.0,
                  color: Colors.red[300],
                ),
                Expanded(
                  child: Text(
                    'Logout',
                    style: TextStyle(
                      fontSize: 18.0,
                      color: Colors.red[800]
                    ),
                  )
                )
              ],
            ),
            onPressed: () async{
              AuthServices auth = new AuthServices();
              bool logout = await auth.logout();
              if(logout == true || logout == false){
                Navigator.of(context).pushAndRemoveUntil(
                  MaterialPageRoute(
                    builder: (context) => LoginPage()
                  ),
                  (Route<dynamic> route) => false
                );
              }
            },
          )
        ],
      )
    );
  }
}