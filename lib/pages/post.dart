import 'package:flutter/material.dart';
import 'package:transparent_image/transparent_image.dart';
import 'package:flutter_html_view/flutter_html_view.dart';

class PostPage extends StatelessWidget {
  final post;
  PostPage({Key key, @required var this.post}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(post['title']['rendered'].toString()),
        backgroundColor: Colors.red,
      ),
      body: Padding(
        padding: EdgeInsets.all(16.0),
        child: ListView(
          children: <Widget>[
            FadeInImage.memoryNetwork(
              placeholder: kTransparentImage,
              image: post["_embedded"]["wp:featuredmedia"][0]["source_url"].toString()
            ),
            Divider(
              height: 16.0,
              color: Colors.transparent,
            ),
            HtmlView(data: post['content']['rendered'].toString())
          ],
        ),
      ),
    );
  }
}