import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:uptime4u/pages/comunicados.dart';
import 'package:uptime4u/pages/home.dart';
import 'package:uptime4u/pages/user.dart';

class MasterPage extends StatefulWidget {
  @override
  _MasterPageState createState() => _MasterPageState();
}

class _MasterPageState extends State<MasterPage> {
  int _currentIndex = 0;
  final List<Widget> _children = [
    HomePage(),
    ComunicadosPage(),
    PlaceholderWidget(Colors.deepOrange),
    PlaceholderWidget(Colors.green)
  ];
  void onTabTapped(int index) {
    setState(() {
      _currentIndex = index;
    });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("UPTIME4U"),
        backgroundColor: Colors.red,
        centerTitle: true,
        actions: <Widget>[
          IconButton(
            icon: Icon(FontAwesomeIcons.userAlt, size: 14.0),
            onPressed: (){
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => UserPage()
                )
              );
            },
          ),
          IconButton(
            icon: Icon(FontAwesomeIcons.solidBell, size: 14.0),
            onPressed: (){
              
            },
          )
        ],
      ),
      backgroundColor: Colors.white,
      body: _children[_currentIndex],
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: this._currentIndex,
        fixedColor: Colors.red[900],
        iconSize: 18.0,
        onTap: onTabTapped,
        items: [
          BottomNavigationBarItem(
            icon: Icon(FontAwesomeIcons.home),
            title: Text('Home')
          ),
          BottomNavigationBarItem(
            icon: Icon(FontAwesomeIcons.newspaper),
            title: Text('News')
          ),
          BottomNavigationBarItem(
            icon: Icon(FontAwesomeIcons.cubes),
            title: Text('Competição')
          ),
          BottomNavigationBarItem(
            icon: Icon(FontAwesomeIcons.userCog),
            title: Text('Suporte')
          )
        ],
      ),
    );
  }
}

class PlaceholderWidget extends StatelessWidget {
  final Color color;

  PlaceholderWidget(this.color);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: color,
    );
  }
}