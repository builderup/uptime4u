import 'package:flutter/material.dart';
import 'package:uptime4u/pages/login.dart';
import 'package:uptime4u/pages/master.dart';
import 'package:uptime4u/services/auth.dart';

AuthServices auth = new AuthServices();
void main() async{
  Widget home = LoginPage();
  bool isLogged = await auth.isLogged();
  if(isLogged == true){
    home = MasterPage();
  }
  print("auth service is logged main " + isLogged.toString());
  // print("auth services get token main " + auth.getToken());
  runApp(
    MaterialApp(
      home: home,
      theme: ThemeData(hintColor: Colors.red, primaryColor: Colors.red),
    )
  );
}